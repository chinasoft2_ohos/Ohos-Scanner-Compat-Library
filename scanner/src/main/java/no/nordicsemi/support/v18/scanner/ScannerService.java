package no.nordicsemi.support.v18.scanner;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.bluetooth.ble.BleScanFilter;
import ohos.event.intentagent.IntentAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ScannerService extends Ability {
    /* package */ static final String EXTRA_PENDING_INTENT = "ohos.permission.GET_NETWORK_INFO";
    /* package */ static final String EXTRA_FILTERS = "EXTRA_FILTERS";
    /* package */ static final String EXTRA_SETTINGS = "EXTRA_SETTINGS";
    /* package */ static final String EXTRA_START = "EXTRA_START";

    private HashMap<IntentAgent, ScanCallback> callbacks;
    private final Object LOCK = new Object();

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        callbacks = new HashMap<>();
    }

    @Override
    public void onCommand(final Intent intent,final boolean restart,final int startId) {
        super.onCommand(intent, restart, startId);
        final IntentAgent callbackIntent = intent.getSequenceableParam(EXTRA_PENDING_INTENT);
        final boolean start = intent.getBooleanParam(EXTRA_START, false);
        final boolean stop = !start;
        if (callbackIntent != null) {
            boolean shouldStop;
            synchronized (LOCK) {
                shouldStop = callbacks.isEmpty();
            }
            if (shouldStop)
                stopScan(callbackIntent);
        }
        boolean knownCallback;
        synchronized (LOCK) {
            knownCallback = callbacks.containsKey(callbackIntent);
        }

        if (start && !knownCallback) {
            final ArrayList<BleScanFilter> filters = intent.getSequenceableArrayListParam(EXTRA_FILTERS);
            final ScanSettings settings = intent.getSequenceableParam(EXTRA_SETTINGS);
            startScan(filters != null ? filters : Collections.<BleScanFilter>emptyList(),
                    settings != null ? settings:new ScanSettings.Builder().build(),
                    callbackIntent);
        } else if (stop && knownCallback) {
            stopScan(callbackIntent);
        }
    }

    private void startScan(List<BleScanFilter> bleScanFilters, ScanSettings scanSettings, IntentAgent callbackIntent) {
        final PendingIntentExecutor executor =
                new PendingIntentExecutor(callbackIntent, scanSettings,this);
        synchronized (LOCK) {
            callbacks.put(callbackIntent, executor);
        }

        try {
            final BluetoothLeScannerCompat scannerCompat = BluetoothLeScannerCompat.getScanner();
        } catch (final Exception e) {
        }
    }

    private void stopScan(final IntentAgent callbackIntent) {
        ScanCallback callback;
        boolean shouldStop;
        synchronized (LOCK) {
            callback = callbacks.remove(callbackIntent);
            shouldStop = callbacks.isEmpty();
        }
        if (callback == null)
            return;
        try {
            final BluetoothLeScannerCompat scannerCompat = BluetoothLeScannerCompat.getScanner();
            scannerCompat.stopScan(callback);
        } catch (final Exception e) {
        }
        if (shouldStop)
            stopScan(callbackIntent);
    }

    @Override
    protected void onStop() {
        final BluetoothLeScannerCompat scannerCompat = BluetoothLeScannerCompat.getScanner();
        for (final ScanCallback callback : callbacks.values()) {
            try {
                scannerCompat.stopScan(callback);
            } catch (final Exception e) {
            }
        }
        callbacks.clear();
        callbacks = null;
        super.onStop();
        super.onStop();
    }
}
