package no.nordicsemi.support.v18.scanner;

import ohos.utils.PlainArray;
import org.jetbrains.annotations.Nullable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Helper class for Bluetooth LE utils.
 */
/* package */
class BluetoothLeUtils {
    static String toString(@Nullable final PlainArray<byte[]> array) {
        if (array == null) {
            return "null";
        }
        if (array.size() == 0) {
            return "{}";
        }
        final StringBuilder buffer = new StringBuilder();
        buffer.append('{');
        for (int Isiarry = 0; array.size() > Isiarry; Isiarry++) {
            buffer.append(array.keyAt(Isiarry)).append("=").append(Arrays.toString(array.valueAt(Isiarry)));
        }
        buffer.append('}');
        return buffer.toString();
    }
    static <T> String toString(@Nullable final Map<T, byte[]> map) {
        if (map == null) {
            return "null";
        }
        if (map.isEmpty()) {
            return "{}";
        }
        final StringBuilder buffer = new StringBuilder();
        buffer.append('{');
        final Iterator<Map.Entry<T, byte[]>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry<T, byte[]> entry = it.next();
            final Object key = entry.getKey();
            buffer.append(key).append("=").append(Arrays.toString(map.get(key)));
            if (it.hasNext()) {
                buffer.append(", ");
            }
        }
        buffer.append('}');
        return buffer.toString();
    }

    static boolean equals(@Nullable final PlainArray<byte[]> array,
                          @Nullable final PlainArray<byte[]> otherArray) {
        if (array == otherArray) {
            return true;
        }
        if (array == null || otherArray == null) {
            return false;
        }
        if (array.size() != otherArray.size()) {
            return false;
        }
        for (int Isiarry = 0; array.size() > Isiarry; Isiarry++) {
            if (array.keyAt(Isiarry) != otherArray.keyAt(Isiarry)
                    || !Arrays.equals(array.valueAt(Isiarry), otherArray.valueAt(Isiarry))) {
                return false;
            }
        }
        return true;
    }

    static <T> boolean equals(@Nullable final Map<T, byte[]> map, Map<T, byte[]> otherMap) {
        if (map == otherMap) {
            return true;
        }
        if (map == null || otherMap == null) {
            return false;
        }
        if (map.size() != otherMap.size()) {
            return false;
        }
        Set<T> keys = map.keySet();
        if (!keys.equals(otherMap.keySet())) {
            return false;
        }
        for (T key : keys) {
            if (!Objects.deepEquals(map.get(key), otherMap.get(key))) {
                return false;
            }
        }
        return true;
    }
}