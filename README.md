# Ohos-Scanner-Compat-Library


#### 项目介绍
- 项目名称：Ohos-Scanner-Compat-Library
- 所属系列：openharmony的第三方组件适配移植
- 功能：蓝牙的操作
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.4.3


#### 效果演示
<img src="https://gitee.com/chinasoft2_ohos/Ohos-Scanner-Compat-Library/raw/master/img/demo.gif"></img>

#### 安装教程
1.在项目根目录下的build.gradle文件中添加
```
allprojects {
  repositories {  
      maven {
          url 'https://s01.oss.sonatype.org/content/repositories/releases/'
      }
  }
}
```
2.在entry模块下的build.gradle文件中添加依赖。
```
dependencies {
  implementation('com.gitee.chinasoft_ohos:Scanner-Compat-Library:1.0.0')
  ......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。

```
    BluetoothLeScannerCompat scanner = BluetoothLeScannerCompat.getScanner();
	ScanSettings settings = new ScanSettings.Builder()
				.setLegacy(false)
				.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
				.setReportDelay(1000)
				.setUseHardwareBatchingIfSupported(true)
				.build();
	List<ScanFilter> filters = new ArrayList<>();
	filters.add(new ScanFilter.Builder().setServiceUuid(mUuid).build());
	scanner.startScan(filters, settings, scanCallback);
```


#### 测试信息   

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
    The Scanner Compat library is available under BSD 3-Clause license. See the LICENSE file for more info.
```